# OpenML dataset: Ipl_predictions2020

https://www.openml.org/d/43700

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Indian Premier League (IPL) is a Twenty20 cricket format league in India. It is usually played in April and May every year. As of 2019, the title sponsor of the game is Vivo. The league was founded by Board of Control for Cricket India (BCCI) in 2008.
Content
Data till Season 11 (2008 - 2019)
matches.csv - Match by match data
Acknowledgements
Data source from 2008-2017 - CricSheet.org and Manas - Kaggle
Data source for 2018-2019 - IPL T20 - Official website
Inspiration
Draw analysis, player/team performance, apply and learn statistical methods on real data
Kernels
-Statistics
Summarizing quantitative data (mean, median, std. deviation, percentile, box plots etc.)
Distributions - Cumulative relative frequency, Normal distribution, PDF, Z-score, empirical rule, binomial distribution, Bernoulli distribution
Bivariate data - Scatter plot, Correlation, Covariance, Least square regression, R-Squared, Root mean square error

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43700) of an [OpenML dataset](https://www.openml.org/d/43700). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43700/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43700/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43700/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

